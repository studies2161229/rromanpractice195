
#include <iostream>


class Animals
{
public:

    // This is not supposed to show in console
    void virtual Voice()
    {
        std::string text = "You fucked up again";
        std::cout << text;
    }
};


class Dog : public Animals
{
public:
    void Voice() override
    {
        std::string text = "\nI am a dog! Woof! Woof!\n";
        std::cout << text;
    }
};


class Cat : public Animals
{
public:
    void Voice() override
    {
        std::string text = "\nI am a cat! Mewo! Meow!\n";
        std::cout << text;
    }
};


class Parrot : public Animals
{
public:
    void Voice() override
    {
        std::string text = "\nSpotting an East India trade vessel the parrot prepares his greasy pirate bosun tone and erupts: \n-Ahoy, matey. All hands on deck!\n";
        std::cout << text;
    }
};


int main()
{
    Dog* p1 = new Dog;
    Cat* p2 = new Cat;
    Parrot* p3 = new Parrot;
    Animals *AnimalsArray[3] = {p1,p2,p3};

    for (int counter = 0; counter < 3; counter++)
    {
        AnimalsArray[counter]->Voice();
        delete AnimalsArray[counter];
    }
}